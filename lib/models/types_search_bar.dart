import 'package:flutter/material.dart';

import '../constants.dart';



class CustomTextField extends StatefulWidget {
  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool show = false;
  FocusNode focusNode = FocusNode();
  String _searchText = "";
  TextEditingController _searchController;
  @override
  Widget build(BuildContext context) {
    return TextField(
          // focusNode: focusNode,
          cursorColor: mbSeconderedColorCpfind,
          cursorWidth: 1,
          controller: _searchController,
          decoration: InputDecoration(
            hoverColor: Colors.black38,
            // prefixIcon: IconButton(
            //   icon: Icon(
            //     Icons.sentiment_very_satisfied,
            //     color: mbSeconderedColorCpfind,
            //   ),
            //   onPressed: () {
            //     // focusNode.unfocus();
            //     // focusNode.canRequestFocus = false;
            //     // setState(() {
            //     //   show = !show;
            //     // });
            //   },
            // ),
            // ? IconButton(
            //     icon: Icon(
            //       AntDesign.message1,
            //       color: mbSeconderedColorCpfind,
            //     ),
            //     onPressed: () {},
            //   )
            suffixIcon: _searchController.text.isEmpty
                ? null
                : IconButton(
                    icon: Icon(
                      Icons.search,
                      color: mbackgrondColor,
                    ),
                    onPressed: () {},
                  ),
            // prefixIcon: IconButton(
            //     icon: _controller.text.isEmpty ? Icon(AntDesign.message1, color: mbSeconderedColorCpfind,) : Icon(Icons.send, color: mbSeconderedColorCpfind,),
            //     onPressed: () {}),
            hintText: "Rechercher...",
            hintStyle: TextStyle(color: Colors.black38),
            filled: true,
            fillColor: Colors.yellow[200],
            enabledBorder: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(25),
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(25),
              borderSide: BorderSide(color: Colors.white),
            )),
        );
  }

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
    _searchController.addListener(() {
      setState(() {
        _searchText = _searchController.text;
      });
    });

  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  // Widget _emojiSelect() {
  //   return EmojiPicker(
  //       rows: 4,
  //       columns: 7,
  //       onEmojiSelected: (emoji, category) {
  //         print(emoji);
  //       });
  // }
}
