import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:secours/screens/types/types.dart';

import 'home/home.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = "/splashScreen";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    Timer(Duration(seconds: 5), () => Navigator.of(context).pushReplacementNamed(Home.routeName));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipOval(
            child: Image(
              width: 200,
              height: 200,
              image: AssetImage("assets/logo.png"),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 50),
          SpinKitRipple(color: Colors.red),
        ]
      ),
    );
  }
}