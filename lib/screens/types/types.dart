import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:secours/models/types_search_bar.dart';

class Types extends StatefulWidget {
  final int index;

  Types({this.index});

  @override
  _TypesState createState() => _TypesState();
}

class _TypesState extends State<Types> {
  DateTime backbuttonpressedTime;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
          appBar: _appBar(context),
          body: Container(
            // padding: EdgeInsets.symmetric(vertical: 20),
            child: FutureBuilder(
              builder: (context, snapshot) {
                var showData = json.decode(snapshot.data.toString());
                return ListView.builder(
              itemCount: showData.length,
              itemBuilder: (context, i) => InkWell(
                onTap: null,
                child: Container(
                  child: Card(
                    elevation: 5,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: ListTile(
                        title: Text(showData[i]['name'],
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        subtitle: Text(
                          showData[i]['description'],
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 13),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
              },
              future: DefaultAssetBundle.of(context).loadString("assets/json/data.json"),
            ),
          )),
    );
  }

  AppBar _appBar(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.red,
      elevation: 10,
      automaticallyImplyLeading: false,
      title: new Text(
        "Premiers Secours",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Icon(Icons.search, color: Colors.white),
              onPressed: null,
            )),
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: PopupMenuButton(
              itemBuilder: (context) => [
                    PopupMenuItem(
                      child: Text("Recherche"),
                    ),
                    PopupMenuItem(
                      child: Text("Langues"),
                    ),
                    PopupMenuItem(
                      child: Text("Aides"),
                    ),
                    PopupMenuItem(
                      child: Text("FAQ"),
                    ),
                  ],
              child: IconButton(
                icon: Icon(Icons.more_vert, color: Colors.white),
                onPressed: null,
              )),
        ),
      ],
    );
  }

  Future<bool> onWillPop() async {
    DateTime currentTime = DateTime.now();
    bool backbutton = backbuttonpressedTime == null ||
        currentTime.difference(backbuttonpressedTime) > Duration(seconds: 3);
    if (backbutton) {
      backbuttonpressedTime = currentTime;
      // Fluttertoast.showToast(
      //   msg: "Double click to exit app",
      // );
      print("Double click to exit app");
      return false;
    }
    return true;
  }
}
