import 'package:flutter/material.dart';
import 'package:secours/constants.dart';
import 'package:secours/utils/helper.dart';

class Body extends StatefulWidget {
  const Body({key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int selectedIndex;

  @override
  void initState() {
    selectedIndex = -1;
    super.initState();
  }

  List<Image> _images = [
    Image.asset("/brulures.jpg", fit: BoxFit.contain),
    Image.asset("/palie.jpg", fit: BoxFit.cover),
    Image.asset("/malaise.jpg", fit: BoxFit.cover),
    Image.asset("/crise.jpg", fit: BoxFit.cover),
  ];

  List<String> name = ["Brulures", "Plaies", "Malaise", "Crise"];

  Widget _buildButton(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: selectedIndex == index
                    ? Theme.of(context).accentColor
                    : Colors.grey[200],
                borderRadius: BorderRadius.circular(25),
              ),
              child: _images[index]),
          Text(
            "${name[index]}",
            style: TextStyle(
                color: selectedIndex == index
                    ? mbSecondebleuColorCpfind
                    : Colors.black,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // var containerHeight = Helper.getScreenHeight(context) * 0.2;
    return SafeArea(
      child: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        child: ListView(
          // padding: const EdgeInsets.symmetric(horizontal: 20),
          children: [
            Container(
              color: mbSecondebleuColorCpfind,
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Premiers Soins",
                      style: Helper.getTheme(context).headline6,
                    ),
                    PopOptionMenu(),
                  ],
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: _images
                      .asMap()
                      .entries
                      .map((MapEntry map) => _buildButton(map.key))
                      .toList(),
                )),
            Container(
              width: double.infinity,
              height: Helper.getScreenHeight(context),
              decoration: BoxDecoration(
                  color: mcardColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50)),
                  boxShadow: [BoxShadow(color: mbSecondebleuColorCpfind)]),
            ),
            // Stack(
            //   children: [
            //     Container(
            //       width: double.infinity,
            //       height: containerHeight,
            //       decoration: BoxDecoration(
            //         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(50), bottomRight: Radius.circular(50)),
            //         color: mbSecondebleuColorCpfind,
            //       ),
            //     ),
            //     Container(
            //       height: Helper.getScreenHeight(context) * 0.9,
            //       child: GridView.builder(
            //         scrollDirection: Axis.vertical,
            //         shrinkWrap: true,
            //         itemCount: 6,
            //         gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            //         itemBuilder: (BuildContext context, int index) => Container(
            //           height: containerHeight,
            //           child: Card(
            //             elevation: 5,
            //             child: Text("card", textAlign: TextAlign.center),
            //           ),
            //         )
            //       ),
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}

enum MenuOption { Langues, Aides, FAQ, Exit }

class PopOptionMenu extends StatelessWidget {
  const PopOptionMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 10),
      child: PopupMenuButton<MenuOption>(
          itemBuilder: (context) => <PopupMenuEntry<MenuOption>>[
                PopupMenuItem(
                  child: Text("Langues"),
                  value: MenuOption.Langues,
                ),
                PopupMenuItem(
                  child: Text("Aides"),
                  value: MenuOption.Aides,
                ),
                PopupMenuItem(
                  child: Text("FAQ"),
                  value: MenuOption.FAQ,
                ),
                PopupMenuItem(
                  child: Text("Exit"),
                  value: MenuOption.Exit,
                ),
              ],
          child: IconButton(
            icon: Icon(Icons.more_vert, color: Colors.white),
            onPressed: null,
          )),
    );
  }
}
