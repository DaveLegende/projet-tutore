import 'package:flutter/material.dart';
import 'package:secours/constants.dart';
import 'package:secours/utils/helper.dart';

import 'components/body.dart';

class Home extends StatefulWidget {
  static const routeName = "/home";
  const Home({key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: _appBar("Premiers Soins"),
      // drawer: Drawer(
      //   child: Container(color: Colors.white),
      // ),
      body: Body(),
    );
  }

  Widget _appBar(String title) {
    return AppBar(
      elevation: 0,
      title: Text(
        "$title",
        style: Helper.getTheme(context).headline6,
      ),
      backgroundColor: mbSecondebleuColorCpfind,
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Icon(Icons.search, color: Colors.white),
              onPressed: null,
            )),
        // PopOptionMenu(),
      ],
    );
  }
}

