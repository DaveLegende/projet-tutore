import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';

import 'components/body.dart';

class Category extends StatelessWidget {
  DateTime backbuttonpressedTime;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: _appBar(context),
        body: Body(),
      ),
    );
  }

  AppBar _appBar(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.red,
      elevation: 10,
      automaticallyImplyLeading: false,
      title: new Text("Premiers Secours", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 10),
            child: IconButton(
              icon: Icon(Icons.search, color: Colors.white),
              onPressed: null,
            )),
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: PopupMenuButton(
              itemBuilder: (context) => [
                    PopupMenuItem(
                      child: Text("Recherche"),
                    ),
                    PopupMenuItem(
                      child: Text("Langues"),
                    ),
                    PopupMenuItem(
                      child: Text("Aides"),
                    ),
                    PopupMenuItem(
                      child: Text("FAQ"),
                    ),
                  ],
              child: IconButton(
                icon: Icon(Icons.more_vert, color: Colors.white),
                onPressed: null,
              )),
        ),
      ],
    );
  }

  Future<bool> onWillPop() async {
    DateTime currentTime = DateTime.now();
    bool backbutton = backbuttonpressedTime == null ||
        currentTime.difference(backbuttonpressedTime) > Duration(seconds: 3);
    if (backbutton) {
      backbuttonpressedTime = currentTime;
      // Fluttertoast.showToast(
      //   msg: "Double click to exit app",
      //   toastLength: Toast.LENGTH_SHORT,
      //   gravity: ToastGravity.CENTER,
      //   timeInSecForIosWeb: 1,
      //   textColor: Colors.white,
      //   backgroundColor: Colors.red,
      //   fontSize: 16,
      // );
      return false;
    }
    return true;
  }
}
