import 'package:flutter/material.dart';
import 'package:secours/screens/types/types.dart';

import 'category_list.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.builder(
          itemCount: listCategory.length,
          itemBuilder: (context, index) => Container(
          
          child: Card(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.4,
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
                    return Types(index: index);
                  }));
                },
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        child: Card(
                          child: Container(
                            color: Colors.grey,
                            height: MediaQuery.of(context).size.height * 0.35,
                            //child: Image.asset("", fit: BoxFit.cover),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.05,
                        child: Center(
                            child: Text("${listCategory[index]}",
                                textScaleFactor: 1,
                                style: TextStyle(fontWeight: FontWeight.bold))),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),),
    );
  }
}
