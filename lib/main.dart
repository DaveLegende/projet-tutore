import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:secours/langage/translations.dart';
import 'package:secours/langage/application.dart';
import 'package:secours/screens/home/home.dart';
import 'package:secours/screens/splash_screens.dart';

import 'constants.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  SpecificLocalizationDelegate _localeOverrideDelegate;

  @override
  void initState(){
    super.initState();
    _localeOverrideDelegate = new SpecificLocalizationDelegate(null);
    ///
    /// Let's save a pointer to this method, should the user wants to change its language
    /// We would then call: applic.onLocaleChanged(new Locale('en',''));
    /// 
    applic.onLocaleChanged = onLocaleChange;
  }

  onLocaleChange(Locale locale){
    setState((){
      _localeOverrideDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'My Application',
      theme: new ThemeData(
        textTheme: TextTheme(
          headline6: TextStyle(
            color: mColoredite,
            fontWeight: FontWeight.bold,
          )
        ),
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: [
        _localeOverrideDelegate,
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      supportedLocales: applic.supportedLocales(),
      home: SplashScreen(),
      routes: {
        SplashScreen.routeName: (context) => SplashScreen(),
        Home.routeName: (context) => Home(),
      },
    );
  }
}

// class MyHomePage extends StatefulWidget {
//   _MyHomePageState createState() => new _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage>{
//   @override
//   Widget build(BuildContext context){
//     return new Scaffold(
//       appBar: _appbar(context),
//       body: Body(),
//     );
//   }
  
//   AppBar _appbar(BuildContext context) {
//     return new AppBar(
//       title: new Text("App_Name"),
//       actions: [
//         Padding(
//           padding: EdgeInsets.only(right: 10),
//           child: IconButton(
//             icon: Icon(Icons.search),
//             onPressed: null,
//           )
//         ),
//         Padding(
//           padding: EdgeInsets.only(right: 10),
//           child: PopupMenuButton(
//             itemBuilder: (context) => [
//               PopupMenuItem(
//                 child: Text("Recherche"),
//               ),

//               PopupMenuItem(
//                 child: Text("Langues"),
//               ),

//               PopupMenuItem(
//                 child: Text("Aides"),
//               ),

//               PopupMenuItem(
//                 child: Text("FAQ"),
//               ),
//             ],
//             child: IconButton(
//             icon: Icon(Icons.more_vert),
//             onPressed: null,
//           )
//           ),
//         ),
//       ],
//     );
//   }
// }


// import 'package:flutter/material.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:secours/langage/translations.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {

  
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),

//       localizationsDelegates: [
//         const TranslationsDelegate(),
//         GlobalMaterialLocalizations.delegate,
//         GlobalWidgetsLocalizations.delegate,
//       ],
//       supportedLocales: [
//           const Locale('en', ''),
//           const Locale('fr', ''),
//       ],

//       home: MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {

//   SpecificLocalizationDelegate _localeOverrideDelegate;

//   @override
//   void initState(){
//     super.initState();
//     _localeOverrideDelegate = new SpecificLocalizationDelegate(null);
//     ///
//     /// Let's save a pointer to this method, should the user wants to change its language
//     /// We would then call: applic.onLocaleChanged(new Locale('en',''));
//     /// 
//     applic.onLocaleChanged = onLocaleChange;
//   }

//   onLocaleChange(Locale locale){
//     setState((){
//       _localeOverrideDelegate = new SpecificLocalizationDelegate(locale);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Container(),
//     );
//   }
// }
